const { Router } = require('express');
const router = Router();
const ShipController = require('../controllers/ship')

router.get('/', ShipController.getShip)
router.get('/add', ShipController.addFormShip)
router.post('/add', ShipController.addShip)
router.get('/delete/:id', ShipController.deleteShip)
router.get('/edit/:id', ShipController.updateShip)
router.get('/:id', ShipController.findById)

module.exports = router;
