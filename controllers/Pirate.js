const { Pirate } = require('../models')

class PirateController {
    static getPirate(req, res) {
        Pirate.findAll()
            .then(result => {
                // console.log(result);
                // res.send(result)
                res.render('Pirate.ejs', { pirates: result })
                
            })
            .catch(err => {
                console.log(err);
                
            })
    }
    static addFormPirate(req, res) {
        res.render('addPirate.ejs');
    }
    static addPirate(req, res) {
        const { name, status, haki } = req.body;
        Pirate.create({
            name,
            status,
            haki
        })
            .then(result => {
                // res.send(result)
                res.redirect('/Pirates')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById(req, res) {
        const id = req.params.id;
        Pirate.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deletePirate(req, res) {
        const id = req.params.id;
        Pirate.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/Pirates')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updatePirate(req, res) {
        const id = req.params.id;
        const { name, status, haki } = req.body;
        Pirate.update({
            name,
            status,
            haki
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = PirateController; 
