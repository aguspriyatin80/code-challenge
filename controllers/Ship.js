const { Ship } = require('../models')

class ShipController {
    static getShip(req, res) {
        Ship.findAll()
            .then(result => {
                // console.log(result);
                
                // res.send(result)
                res.render('ship.ejs', { Ships: result })
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addFormShip(req, res) {
        res.render('addShip.ejs');
    }
    static addShip(req, res) {
        const { name, type, power } = req.body;
        Ship.create({
            name,
            type,
            power
        })
            .then(result => {
                // res.send(result)
                res.redirect('/Ships')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById(req, res) {
        const id = req.params.id;
        Ship.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteShip(req, res) {
        const id = req.params.id;
        Ship.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/Ships')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updateShip(req, res) {
        const id = req.params.id;
        const { name, type, power } = req.body;
        Ship.update({
            name,
            type,
            power
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = ShipController; 
